#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include "math.h"

#include "GEOKO.h"

	GEOKO::GEOKO(double xBr, double xLa) {

		abstandMitte = -1;
		Br = xBr;
		BrGr = (int)(xBr / 3600);
		BrMin = std::fmod(xBr, 3600) / 60;
		BrSec = std::fmod((std::fmod(xBr, 3600) * 60), 60);
		La = xLa;
		LaGr = (int)(xLa / 3600);
		LaMin = std::fmod(xLa, 3600) / 60;
		LaSec = std::fmod((std::fmod(xLa, 3600) * 60), 60);

	};

	GEOKO::GEOKO(void) {

		abstandMitte = -1;
		BrGr = -1;
		BrMin = -1;
		BrSec = -1;
		LaGr = -1;
		LaMin = -1;
		LaSec = -1;

	};

	//GETTER
	double GEOKO::getBr(){
		return this->Br;
	};

	double GEOKO::getLa(){
		return this->La;
	};

	int GEOKO::getBrGr() {
		return this->BrGr;
	};

	int GEOKO::getBrMin() {
		return this->BrMin;
	};

	double GEOKO::getBrSec() {
		return this->BrSec;
	};

	int GEOKO::getLaGr() {
		return this->LaGr;
	};

	int GEOKO::getLaMin() {
		return this->LaMin;
	};

	double GEOKO::getLaSec() {
		return this->LaSec;
	};

	

	//SETTER
	void GEOKO::setBr(double xBr){
		this->Br = xBr;
	};

	void GEOKO::getLa(double xLa){
		this->La = xLa;
	};

	void GEOKO::setBrGr(int eBrGr) {
		this->BrGr = eBrGr;
	};

	void GEOKO::setBrMin(int eBrMin) {
		this->BrMin = eBrMin;
	};

	void GEOKO::setBrSec(double eBrSec) {
		this->BrSec = eBrSec;
	};

	void GEOKO::setLaGr(int eLaGr) {
		this->LaGr = eLaGr;
	};

	void GEOKO::setLaMin(int eLaMin) {
		this->LaMin = eLaMin;
	};

	void GEOKO::setLaSec(double eLaSec) {
		this->LaSec = eLaSec;
	};
	//... ? ? ? ...

	bool GEOKO::operator > (GEOKO &GK) {
		if (this->abstandMitte > GK.abstandMitte) {
			return true;
		}
		else {
			return false;
		}
	};
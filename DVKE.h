#pragma once

#include "GEOKO.h"

class DVKE {

protected:
	DVKE * N;
	DVKE * V;
	int Index;
	GEOKO * Data;

public:
	DVKE(GEOKO * eData, DVKE* eN, DVKE* eV);

	DVKE(GEOKO * eData);

	DVKE();
	
	void setData(GEOKO* xGeo);

	GEOKO* getData();

	void setIndex(int xIndex);

	int getIndex();

	DVKE* getNachfolger();

	void setNachfolger(DVKE * eNachfolger);

	DVKE* getVorgaenger();

	void setVorgaenger(DVKE* eVorgaenger);
	//... ? ? ?

}; //...class DVKE
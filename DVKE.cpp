#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include "math.h"

#include "DVKE.h"
#include "GEOKO.h"


	DVKE::DVKE(GEOKO * eData, DVKE* eN, DVKE* eV) :
		Data(eData), N(eN), V(eV) {};

	DVKE::DVKE(GEOKO * eData) {
		this->Data = eData;
	}

	DVKE::DVKE() {

		this->N = NULL;
		this->V = NULL;
		this->Data = NULL;

	}
	void DVKE::setData(GEOKO* xGeo){
		Data = xGeo;
	}

	GEOKO* DVKE::getData(){
		return this->Data;
	}


	void DVKE::setIndex(int xIndex) {
		Index = xIndex;
	}

	int DVKE::getIndex() {
		return this->Index;
	}

	DVKE* DVKE::getNachfolger() {
		return this->N;
	}

	void DVKE::setNachfolger(DVKE * eNachfolger) {
		this->N = eNachfolger;
	}

	DVKE* DVKE::getVorgaenger() {
		return this->V;
	}

	void DVKE::setVorgaenger(DVKE* eVorgaenger) {
		this->V = eVorgaenger;
	}


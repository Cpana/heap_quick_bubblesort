#pragma once
#include "DVKE.h"
#include "GEOKO.h"

class DVK : private DVKE {

	int Anz;
	int Max;
	DVKE* Start;
	DVKE* Ende;
	GEOKO* Mittelpunkt;
	DVKE* Index[10000];

public:

	DVK(int Anzahl);
	
	DVK();

	void setMax(int xMax);

	int getMax();

	int getAnz();

	void setStart(DVKE* xStart);

	DVKE* getStart();
	
	DVKE* getListenelement(int position);

	void setEnde(DVKE* xEnde);

	DVKE* getEnde();

	bool anhaengen(GEOKO* xGeo);

	void vertausche(int First, int Second);
	
	double abstand(GEOKO *xGeo);

	void berechneAbstaende();
	
	void erzeugeMaxHeap(int anz);
	
	void heapSort();
	
	void quickSort(int links, int rechts);
	
	void bubbleSort();

};

#include <cmath>
#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include "math.h"

#include "DVKE.h"
#include "GEOKO.h"
#include "DVK.h"

	DVK::DVK(int Anzahl) {
		Max = Anzahl;
		Anz = 0;
		Start = NULL;
		Ende = NULL;
		Mittelpunkt = new GEOKO();
	};

	void DVK::setMax(int xMax) {
		this->Max = xMax;
	};

	int DVK::getMax() {
		return this->Max;
	};

	int DVK::getAnz() {
		return this->Anz;
	};

	void DVK::setStart(DVKE* xStart) {
		this->Start = xStart;
	};

	DVKE* DVK::getStart() {
		return this->Start;
	};

	DVKE* DVK::getListenelement(int position){
		return this->Index[position];
	}

	void DVK::setEnde(DVKE* xEnde) {
		this->Ende = xEnde;
	};

	DVKE* DVK::getEnde() {
		return Ende;
	};

	bool DVK::anhaengen(GEOKO* xGeo) {

		if (Anz == 0) {

			DVKE* neuesElement = new DVKE(xGeo);

			//Setzte bei neuem Element richtige Nachfolger
			neuesElement->setVorgaenger(this->getStart());
			neuesElement->setNachfolger(this->getEnde());
			neuesElement->setIndex(Anz);
			//Setzte neues Element als Start und gleichzeitig Ende, da nur ein Element
			setStart(neuesElement);
			setEnde(neuesElement);
			
			
			Mittelpunkt->setBrGr((int)(xGeo->getBr() / 3600));
			Mittelpunkt->setBrMin(std::fmod(xGeo->getBr(), 3600) / 60);
			Mittelpunkt->setBrSec(std::fmod((std::fmod(xGeo->getBr(), 3600) * 60), 60));

			Mittelpunkt->setLaGr((int)(xGeo->getLa() / 3600));
			Mittelpunkt->setLaMin(std::fmod(xGeo->getLa(), 3600) / 60);
			Mittelpunkt->setLaSec(std::fmod((std::fmod(xGeo->getLa(), 3600) * 60), 60));
			
			
			Index[Anz] = neuesElement;
			//Zaehler tatsaechlicher Elemente in der Liste 
			this->Anz = this->Anz + 1;
			return true;

		}
		else {
			DVKE* neuesElement = new DVKE(xGeo);
			DVKE* tempVorgaenger = this->getEnde();
			double neuBr;
			double neuLa;
			

			//Im neuen Element V und N setzen
			neuesElement->setVorgaenger(tempVorgaenger);
			neuesElement->setNachfolger(NULL);
			//Beim Vorgaenger neuen N setzen
			tempVorgaenger->setNachfolger(neuesElement);
			neuesElement->setIndex(Anz);
			//Neues Ende setzen
			this->setEnde(neuesElement);
			
			neuBr = ((Mittelpunkt->getBr() * Anz) + xGeo->getBr()) / (Anz+1);
			neuLa = ((Mittelpunkt->getLa() * Anz) + xGeo->getLa()) / (Anz+1);
			
			Mittelpunkt->setBrGr((int)(neuBr / 3600));
			Mittelpunkt->setBrMin(std::fmod(neuBr, 3600) / 60);
			Mittelpunkt->setBrSec(std::fmod((std::fmod(neuBr, 3600) * 60), 60));

			Mittelpunkt->setLaGr((int)(neuLa / 3600));
			Mittelpunkt->setLaMin(std::fmod(neuLa, 3600) / 60);
			Mittelpunkt->setLaSec(std::fmod((std::fmod(neuLa, 3600) * 60), 60));
			
			
			
			Index[Anz] = neuesElement;
			
			this->Anz = this->Anz + 1;
			
			
			
			/*
			std::cout << "\n";
			std::cout << "Neuer Mittelpunkt: \n";
			std::cout << "\n";
			std::cout << "BrGr : " << Mittelpunkt->getBrGr() << "\n";
			std::cout << "BrMin: " << Mittelpunkt->getBrMin() << "\n";
			std::cout << "BrSec: " << Mittelpunkt->getBrSec() << "\n";
			std::cout << "\n";
			std::cout << "LaGr:  " << Mittelpunkt->getLaGr() << "\n";
			std::cout << "BrMin: " << Mittelpunkt->getLaMin() << "\n";
			std::cout << "BrSec: " << Mittelpunkt->getLaSec()<< "\n";
			std::cout << "\n";
			std::cout << "\n";
			*/
			

			return true;

		}

	};

	void DVK::vertausche(int First, int Second) {
		
		DVKE* A = Index[First];
		int IndexA = A->getIndex();
		
		DVKE* B = Index[Second];
		int IndexB = B->getIndex();
		
		DVKE* C;

		
		if(IndexA > IndexB){
			
			IndexA = Second;
			IndexB = First;
			
			A = Index[Second];
			B = Index[First];

		} 
		
		//Fall 6
		//Wenn das erste Element der Liste mit dem Zweiten getauscht werden muss und die Liste mehr als 2 Elemente hat.
		if (IndexA == 0 && IndexB == 1 && this->Anz > 2) {
			
			DVKE* nachfolgerVonB;
			nachfolgerVonB = B->getNachfolger();

			this->setStart(B);

			A->setVorgaenger(B);
			A->setNachfolger(nachfolgerVonB);

			B->setVorgaenger(NULL);
			B->setNachfolger(A);

			nachfolgerVonB->setVorgaenger(A);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;

		}
		//Fall 8
		//Wenn das erste Element der Liste mit dem Zweiten getauscht werden muss und die Liste nur 2 Elemente hat.
		else if (IndexA == 0 && IndexB == 1 && this->Anz == 2) {

			this->setStart(B);
			this->setEnde(A);

			A->setVorgaenger(B);
			A->setNachfolger(NULL);

			B->setVorgaenger(NULL);
			B->setNachfolger(A);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;

		}
		//Fall 2
		//Wenn das erste Element mit einem mittendrin getauscht werden soll
		else if (IndexA == 0 && IndexB != 1 && ((IndexB+1) != this->Anz)) {

			DVKE* nachfolgerVonA = A->getNachfolger();

			DVKE* vorgaengerVonB = B->getVorgaenger();
			DVKE* nachfolgerVonB = B->getNachfolger();
			
			nachfolgerVonA->setVorgaenger(B);

			vorgaengerVonB->setNachfolger(A);
			nachfolgerVonB->setVorgaenger(A);

			A->setVorgaenger(vorgaengerVonB);
			A->setNachfolger(nachfolgerVonB);

			B->setVorgaenger(NULL);
			B->setNachfolger(nachfolgerVonA);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;
			
			this->setStart(B);

		}
		//Fall 3
		//Wenn mittendrin und das Letzte
		else if (IndexA < IndexB && (IndexB+1) == this->Anz && IndexA != 0 && (IndexA+1)!=IndexB ) {

			DVKE* vorgaengerVonA = A->getVorgaenger();
			DVKE* nachfolgerVonA = A->getNachfolger();

			DVKE* vorgaengerVonB = B->getVorgaenger();

			vorgaengerVonA->setNachfolger(B);
			nachfolgerVonA->setVorgaenger(B);

			
			vorgaengerVonB->setNachfolger(A);

			A->setVorgaenger(vorgaengerVonB);
			A->setNachfolger(NULL);

			B->setVorgaenger(vorgaengerVonA);
			B->setNachfolger(nachfolgerVonA);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;
			
			this->setEnde(A);

		}
		//Fall 1
		//Wenn 2 Elemente getauscht werden die nicht hintereinander liegen und sich nicht am Anfang oder Ende befinden (2 Mittendrin)
		else if ((IndexA < IndexB) && (IndexA != 0 && (IndexB+1) != this->Anz) && ((IndexA + 1) != IndexB)) {

			DVKE* vorgaengerVonA = A->getVorgaenger();
			DVKE* nachfolgerVonA = A->getNachfolger();

			DVKE* vorgaengerVonB = B->getVorgaenger();
			DVKE* nachfolgerVonB = B->getNachfolger();

			A->setVorgaenger(vorgaengerVonB);
			A->setNachfolger(nachfolgerVonB);

			B->setVorgaenger(vorgaengerVonA);
			B->setNachfolger(nachfolgerVonA);

			vorgaengerVonA->setNachfolger(B);
			nachfolgerVonA->setVorgaenger(B);

			vorgaengerVonB->setNachfolger(A);
			nachfolgerVonB->setVorgaenger(A);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;

		}
		//Fall 4
		//Erste und letzte Elemente mit Anzahl der Elemente > 2
		else if ((IndexA == 0 && (IndexB+1) == this->Anz) && this->Anz > 2) {

			DVKE* nachfolgerVonA = A->getNachfolger();
			DVKE* vorgaengerVonB = B->getVorgaenger();

			nachfolgerVonA->setVorgaenger(B);
			vorgaengerVonB->setNachfolger(A);

			A->setVorgaenger(vorgaengerVonB);
			A->setNachfolger(NULL);
			
			B->setVorgaenger(NULL);
			B->setNachfolger(nachfolgerVonA);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			this->setStart(B);
			this->setEnde(A);
			
			Index[IndexA] = B;
			Index[IndexB] = A;

		}
		//Fall 5
		//2 Hintereinanderliegende Mittendrin
		else if ((IndexA < IndexB) && ((IndexA + 1) == IndexB) && IndexA != 0 && (IndexB+1) != this->Anz) {

			DVKE* vorgaengerVonA = A->getVorgaenger();

			DVKE* nachfolgerVonB = B->getNachfolger();

			vorgaengerVonA->setNachfolger(B);
			nachfolgerVonB->setVorgaenger(A);

			A->setVorgaenger(B);
			A->setNachfolger(nachfolgerVonB);

			B->setVorgaenger(vorgaengerVonA);
			B->setNachfolger(A);
			
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;

		}
		//Fall 7
		//Hintereinander und letztes Element
		else if ( (IndexA + 1) == IndexB && this->Anz == (IndexB+1) ) {
			
			DVKE* vorgaengerVonA = A->getVorgaenger();
						
			this->setEnde(A);
			
			vorgaengerVonA->setNachfolger(B);
					
			A->setVorgaenger(B);
			A->setNachfolger(NULL);

			B->setVorgaenger(vorgaengerVonA);
			B->setNachfolger(A);
				
			A->setIndex(IndexB);
			B->setIndex(IndexA);
			
			Index[IndexA] = B;
			Index[IndexB] = A;


		} else if(IndexA == IndexB){
		
		} else {
			
			std::cout << "--------------------------------------\n";
			std::cout << "INDEX A:\n";
			std::cout << IndexA;
			std::cout << "--------------------------------------\n";
			
			std::cout << "--------------------------------------\n";
			std::cout << "INDEX B:\n";
			std::cout << IndexB;
			std::cout << "--------------------------------------\n";
			
			
		 	std::cout << "--------------------------------------\n";
			std::cout << "ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR!\n";
			std::cout << "--------------------------------------\n";
		
		}

	};

	
	double DVK::abstand(GEOKO *xGeo) {
		
  		return sqrt(pow(this->Mittelpunkt->getBrGr() - xGeo->getBrGr(),2) + pow(this->Mittelpunkt->getLaGr() - xGeo->getLaGr(),2) );	
 	};

	void DVK::berechneAbstaende(){
 	
		for(int i=0;i<this->Anz;i++) {
			this->Index[i]->getData()->abstandMitte = abstand(this->Index[i]->getData());  	
		}
 	};

	void DVK::erzeugeMaxHeap(int anz){
		int K = ((anz-1)/2);
		DVKE* Knoten;
		DVKE* BlattEins;
		DVKE* BlattZwei;
		DVKE* Groesseres;
		
		bool nixpassiert = false;
		
		while(!nixpassiert){
			for(int i = 1; i<=K; i++){
					
				Knoten = Index[i-1];
					
				if (i*2 > this->Anz){
				
					Groesseres = Index[(i*2)-1];
					
				} else {
					/*
					std::cout << "Knoten AbstMitte: " << Knoten->getData()->abstandMitte << "\n";
					std::cout << "Index :" << Knoten->getIndex() <<"\n";
					*/
					BlattEins = Index[(i*2)-1];
					/*
					std::cout << "BlattEins AbstMitte: " << BlattEins->getData()->abstandMitte << "\n";
					std::cout << "Index :" << BlattEins->getIndex() <<"\n";
					*/
					BlattZwei = Index[(i*2)];
					/*
					std::cout << "BlattZwei AbstMitte: " << BlattZwei->getData()->abstandMitte << "\n";
					std::cout << "Index :" << BlattZwei->getIndex() <<"\n";
					*/
					if (BlattEins->getData()->abstandMitte > BlattZwei->getData()->abstandMitte){
						Groesseres = BlattEins;
					}else{
						Groesseres = BlattZwei;
					}
				}
				/*
				std::cout << "Groesser AbstMitte: " << Groesseres->getData()->abstandMitte << "\n";
				std::cout << "Index :" << Groesseres->getIndex() <<"\n";
				*/
				/*
				bool xl = Groesseres->getData()->abstandMitte > Knoten->getData()->abstandMitte;
				std::cout << xl;
				*/
				
				if(Groesseres->getData()->abstandMitte > Knoten->getData()->abstandMitte ){
					vertausche(Knoten->getIndex(),Groesseres->getIndex());
				} else {
					nixpassiert = true;
				}
			}	
		}	
	};
			
	
	void DVK::heapSort(){
		
	   while((this->Anz-1) > 1){
		  this->erzeugeMaxHeap(this->Anz);
		  this->vertausche(0,this->Anz-1);
		  this->Anz--;
	   }
	};


	void DVK::quickSort(int links, int rechts) {
		if(links < this->Anz && rechts<this->Anz){
		int i = links; //0
		int j = rechts; //199

		DVKE* pivot = Index[(((links+1+rechts+1))/2)-1]; //99 //Pivot Element -> Das Mittige0+199/2 = 99,5 = 99 
		
		/*
		std::cout << pivot->getData()->abstandMitte << "\n";
		std::cout << "__________\n";
		std::cout << i << "\n";
		std::cout << j << "\n";
		std::cout << "__________\n";	
		*/
			
		DVKE* linkesElement = Index[i]; ///Element der Liste mit Index 0
		DVKE* rechtesElement = Index[j]; //Element der Liste mit Index199

		
			
			while(i<=j){
				
				while(Index[i]->getData()->abstandMitte < pivot->getData()->abstandMitte )i++; //Finde Unpassendes Element links von Pivot
				while(Index[j]->getData()->abstandMitte > pivot->getData()->abstandMitte )j--; //Finde Unpassendes Element rechts von Pivot
				
				if (i <= j) {
					//std::cout << "Tausche " << i << " mit " << j << "\n";
					vertausche(i, j); //Tausche
					i++;
					j--;
				}
				

				}
			if(links < j) quickSort(links,j);
			if(i < rechts) quickSort(i,rechts);
			
			
		}
		
	};


	void DVK::bubbleSort(){
		for(int n = this->Anz; n>1; n--){
			for (int i = 0; i<n-1; i++){
				if(Index[i]->getData()->abstandMitte > Index[i+1]->getData()->abstandMitte){
					vertausche(i,i+1);
				}
			}
		}
	}


	



#pragma once

class GEOKO {

	int BrGr;
	int BrMin;
	double BrSec;
	int LaGr;
	int LaMin;
	double LaSec;
	double La;
	double Br;

public:
	double abstandMitte;

	GEOKO(double xBr, double xLa);

	GEOKO(void);

	//GETTER
	double getBr();

	double getLa();
	
	int getBrGr();

	int getBrMin();

	double getBrSec();

	int getLaGr();

	int getLaMin();

	double getLaSec();

	//SETTER
	void setBr(double xBr);

	void getLa(double xLa);
	
	void setBrGr(int eBrGr);

	void setBrMin(int eBrMin);

	void setBrSec(double eBrSec);

	void setLaGr(int eLaGr);

	void setLaMin(int eLaMin);

	void setLaSec(double eLaSec);
	//... ? ? ? ...

	bool operator > (GEOKO &GK);

}; //...class GEOKO
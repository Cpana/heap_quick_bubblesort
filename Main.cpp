#include "DVK.h"
#include "DVKE.h"
#include "GEOKO.h"


#include <cmath>
#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include "math.h"
#include <iomanip>
#include <time.h>


int main() {
	
	double Br, La;
	
	std::ifstream fileHeapSort;
	std::ifstream fileQuickSort;
	std::ifstream fileBubbleSort;
	
	std::ofstream ofileHeapSort;
	std::ofstream ofileQuickSort;
	std::ofstream ofileBubbleSort;
	
	GEOKO* neu;
	
	int anzahlElemente = 10000;
	
	DVK* listeQuickSort = new DVK(anzahlElemente);
	DVK* listeHeapSort = new DVK(anzahlElemente);
	DVK* listeBubbleSort = new DVK(anzahlElemente);
	
	DVKE* element;
	
	std::string br;
	
	std::string la;
	
	DVKE* a;
	DVKE* b;
	
	clock_t zeit1, zeit2;
	
	int auswahl;
	
	
	
	
	do{
		
		std::cout<<"   >>>>>>>>>>>Menue<<<<<<<<<"<< "\n";
		std::cout<<"1) Heap Sort mit Zeitmessung"<< "\n";
		std::cout<<"2) Quick Sort  mit Zeitmessung"<< "\n";
		std::cout<<"3) Bubble Sort  mit Zeitmessung"<< "\n";
		std::cout<<"4) Ende                   "<< "\n";
	
		std::cin>>auswahl;
	
		switch(auswahl){
			
			 case 1: 	  
				fileHeapSort.open("DatenHeapSort.csv");
				
				for (int i = 0; i < anzahlElemente; i++) {
					getline(fileHeapSort, br, ',');
					
					//Macht macht Leerzeichen weg	
					for (unsigned long i = 0; i < br.size();) if (br[i] == ' ') br.erase(i, 1); else i++;
					Br = atof(br.c_str());
					getline(fileHeapSort, la, ';');

					//Macht macht Leerzeichen weg
					for (unsigned long i = 0; i < la.size();) if (la[i] == ' ') la.erase(i, 1); else i++;
					La = atof(la.c_str());
					neu = new GEOKO(Br, La);

					listeHeapSort->anhaengen(neu);
					
				}		
				listeHeapSort->berechneAbstaende();
				
				
				zeit1 = clock();
				
				listeHeapSort->heapSort();
				
				zeit2 = clock();
				
				std::cout << "Vergangene Zeit beim Sortieren der Liste mit " << anzahlElemente << " Elementen durch HeapSort: " << (zeit2-zeit1) << "\n";
				
				fileHeapSort.close();
				
				
				ofileHeapSort.open("SpeicherHeapSort.csv");
	
				for(int i = 0; i<anzahlElemente; i++){
					ofileHeapSort << "   " << std::setprecision(9) << listeHeapSort->getListenelement(i)->getData()->getBr() << "   ,   " << std::setprecision(9) << listeHeapSort->getListenelement(i)->getData()->getLa() << "   ,   " <<  std::setprecision(9) << listeHeapSort->getListenelement(i)->getData()->abstandMitte  << ";" << std::endl;
				}
				std::cout << "Sortierte Liste wurde in der Datei: SpeicherHeapSort.csv Gespeichert \n";
				ofileHeapSort.close();
				break;

			 case 2: 
				
				fileQuickSort.open("DatenQuickSort.csv");
				
				for (int i = 0; i < anzahlElemente; i++) {
					getline(fileQuickSort, br, ',');
					
					//Macht macht Leerzeichen weg	
					for (unsigned long i = 0; i < br.size();) if (br[i] == ' ') br.erase(i, 1); else i++;
					Br = atof(br.c_str());
					getline(fileQuickSort, la, ';');

					//Macht macht Leerzeichen weg
					for (unsigned long i = 0; i < la.size();) if (la[i] == ' ') la.erase(i, 1); else i++;
					La = atof(la.c_str());
					neu = new GEOKO(Br, La);

					listeQuickSort->anhaengen(neu);
					
				}		
				listeQuickSort->berechneAbstaende();
				
				zeit1 = clock();
				
				listeQuickSort->quickSort(0, listeQuickSort->getAnz()-1);

				zeit2 = clock();
				
				std::cout << "Vergangene Zeit beim Sortieren der Liste mit " << anzahlElemente << " Elementen durch QuickSort: " << (zeit2-zeit1) << "\n";
				
				fileQuickSort.close(); 
				
				
				
				ofileQuickSort.open("SpeicherQuickSort.csv");
	
				for(int i = 0; i<anzahlElemente; i++){
					ofileQuickSort << "   " << std::setprecision(9) << listeQuickSort->getListenelement(i)->getData()->getBr() << "   ,   " << std::setprecision(9) << listeQuickSort->getListenelement(i)->getData()->getLa() << "   ,   " <<  std::setprecision(9) << listeQuickSort->getListenelement(i)->getData()->abstandMitte  << ";" << std::endl;
				}
				
				std::cout << "Sortierte Liste wurde in der Datei: SpeicherQuickSort.csv Gespeichert \n";
				ofileQuickSort.close();
				break;

			 case 3: 
				fileBubbleSort.open("DatenBubbleSort.csv");
				
				for (int i = 0; i < anzahlElemente; i++) {
					getline(fileBubbleSort, br, ',');
					
					//Macht macht Leerzeichen weg	
					for (unsigned long i = 0; i < br.size();) if (br[i] == ' ') br.erase(i, 1); else i++;
					Br = atof(br.c_str());
					getline(fileBubbleSort, la, ';');

					//Macht macht Leerzeichen weg
					for (unsigned long i = 0; i < la.size();) if (la[i] == ' ') la.erase(i, 1); else i++;
					La = atof(la.c_str());
					neu = new GEOKO(Br, La);

					listeBubbleSort->anhaengen(neu);
					
				}		
				listeBubbleSort->berechneAbstaende();
				
				zeit1 = clock();
				
				listeBubbleSort->bubbleSort();

				zeit2 = clock();
				
				std::cout << "Vergangene Zeit beim Sortieren der Liste mit " << anzahlElemente << " Elementen durch BubbleSort: " << (zeit2-zeit1) << "\n";
				
				fileBubbleSort.close(); 
				
				
				ofileBubbleSort.open("SpeicherBubbleSort.csv");
	
				for(int i = 0; i<anzahlElemente; i++){
					ofileBubbleSort << "   " << std::setprecision(9) << listeBubbleSort->getListenelement(i)->getData()->getBr() << "   ,   " << std::setprecision(9) << listeBubbleSort->getListenelement(i)->getData()->getLa() << "   ,   " <<  std::setprecision(9) << listeBubbleSort->getListenelement(i)->getData()->abstandMitte  << ";" << std::endl;
				}
				
				std::cout << "Sortierte Liste wurde in der Datei: SpeicherBubbleSort.csv Gespeichert \n";
				ofileBubbleSort.close();
				
				break;  
		
			 case 4 : 
				break; 

		}

	} while(auswahl!=4);
}
	

	

